package client;

import java.util.Map;

import org.kie.dmn.api.core.DMNContext;
import org.kie.dmn.api.core.DMNResult;
import org.kie.server.api.marshalling.MarshallingFormat;
import org.kie.server.api.model.ServiceResponse;
import org.kie.server.client.DMNServicesClient;
import org.kie.server.client.KieServicesClient;
import org.kie.server.client.KieServicesConfiguration;
import org.kie.server.client.KieServicesFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Main {

	final static Logger log = LoggerFactory.getLogger(Main.class);

	private static final String URL = "http://localhost:8090/rest/server";
	private static final String user = System.getProperty("username", "user");
	private static final String password = System.getProperty("password", "user");
	private static final String CONTAINER = "decision-kjar-1_0-SNAPSHOT";

	public static void main(String[] args) {
		Main clientApp = new Main();

		long start = System.currentTimeMillis();

		clientApp.evaluateDMN();

		long end = System.currentTimeMillis();
		System.out.println("elapsed time: " + (end - start));
	}

	private void evaluateDMN() {
		KieServicesClient client = getClient();
		DMNServicesClient dmnClient = client.getServicesClient(DMNServicesClient.class);

		String namespace = "http://www.redhat.com/definitions/avios-poc";
		String modelName = "avios";

		DMNContext dmnContext = dmnClient.newContext();

		dmnContext.set("Partner", "BA");
		dmnContext.set("Product", "Flight");
		dmnContext.set("Sub Product", "ET");
		dmnContext.set("Price", "1000");

		ServiceResponse<DMNResult> result = dmnClient.evaluateAll(CONTAINER, namespace, modelName, dmnContext);
		System.out.println(result.getResult().getContext().get("Calculate discount and avios points"));

	}

	private KieServicesClient getClient() {
		KieServicesConfiguration config = KieServicesFactory.newRestConfiguration(URL, user, password);

		// Configuration for JMS
		// KieServicesConfiguration config =
		// KieServicesFactory.newJMSConfiguration(connectionFactory, requestQueue,
		// responseQueue, username, password)

		Map<String, String> headers = null;
		config.setHeaders(headers);
		config.setMarshallingFormat(MarshallingFormat.JSON);
		KieServicesClient client = KieServicesFactory.newKieServicesClient(config);

		return client;
	}

}
